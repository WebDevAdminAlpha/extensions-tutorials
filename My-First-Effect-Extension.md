# Resources

* [Tutorial Video](no-link)
* [Template Files](uploads/25063b4ae6c3396fcda428105c5cff89/template_effect.zip)
* [Example Code](no-link)
* [Example SVG](no-link)

# Introduction

Effect extensions take the svg from Inkscape, modify it in some way and pass the modified version back to Inkscape to be rendered onto the canvas. This can be very powerful, allowing everything from randomising colours to manipulating path elements in external programs.

We are going to write an effect extension that will change any path with an even number of points to a blue stroke and any path with an odd number of points to red. 

# Step One

Extract the `Effect Extension Template` files into a folder on your computer. You should have two files, one inx file and one python file. Move or link these files into your extensions directory as you would when installing extensions manually. This is the directory listed at `Edit > Preferences > System: User extensions`. 

Edit the inx file in a text editor and change the name of the extension to 'My First Extension' and the id to 'org.inkscape.tutorial.my_first_extension' by changing these lines near the top:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
  <name>My First Extension</name>
  <id>org.inkscape.my_first_extension</id>
    [...]
```

Toward the end of the .inx file, change the submenu to `Effect` specify that this extension will be listed in the `Effect` submenu under the `Extensions` menu:

```xml
    [...]
    <effect>
        <!--object-type>path</object-type-->
        <effects-menu>
            <submenu name="Effect"/>
        </effects-menu>
    </effect>
    [...]
```

# Step Two

Next, open `my_effect_extension.py` in your text editor. You can see this is an inkex extension because it contains a python class that inherits from `inkex.EffectExtension`. Change the class name to MyFirstExtension:

```python
    [...]
    class MyFirstExtension(inkex.EffectExtension):
    [...]
```

Reflect this change down in the `__main__` section of the code by changing the class name to MyFirstExtension there:

```python
[...]
if __name__ == '__main__':
    MyFirstExtension().run()
```

When a standard inkex-based python effect extension is run, it will call a method called `effect()` on your extension's class. So, most of the code you need to write will go there. Edit `my_effect_extension.py`'s `effect()` method to look like the following, **being sure that the indentation is correct so that `effect()` is recognized as a method of the MyFirstExtension class**:

```python
    def effect(self):
        for elem in self.svg.get_selected():
            elem.style['fill'] = 'red'
```

## Code Explanation

Firstly, we need to loop through each of the selected paths. The first line of `effect()` does this - `self.svg` is the core API for interacting with the SVG file itself. Its `get_selected()` method returns the currently selected objects, and the `inkex.PathElement` parameter limits it to selected paths. If other objects, such as a text object or rectangle, are selected, these are ignored. So, for each iteration of the loop, `elem` will contain one of the selected path objects.

The second line sets an attribute `inkscape:modified_by_tutorial` on the xml element `elem`. The attribute API will handle the `inkscape` namespace for us, so we can use a simple colon to indicate the namespace. This is how all non-special attributes are set and gotten. But on top of this simple API we don't have to worry about parsing the path, transform or the style attributes. Instead the inkex API does all the parsing for us and provides us with a way to change styles, modify paths and even do transformations without any of the manual parsing of the previous APIs.

Then we set a stroke-width of 2, using the standard style API which is assigning into a simple ordered dictionary. 

Next we use the Color API to create two colors. We're using RGBA to force setting the opacity at the same time. Stroke and stroke opacity are two different style properties, so we can't set them with a simple assignment the way we did width. The `set_color()` API will handle this correctly for us. 

Checking the number of nodes uses the `path` API. We're using it very simply to count the number of nodes in the path and see if it's odd or even.

# Final Step

That's it! There's no need to set, save or do anything else as we've modified the style in place. Most of the APIs operate in place, but if you needed to set the path, style or transform you could just use `elem.path = my_replacement_path` or similar for these special properties.

Save your python script, and re-launch Inkscape. If inkscape was already open, close it first. You should find your new extension available in the `Effect` menu. 

Draw some shapes with the pen tool or download the exmaple svg file, select some of the shapes and use the extension. You should see the stroke change for each of the objects selected. 

# Sample

* Here we see a path with 4 points (left), a path with 5 points, and a circle. We expect the extension to change the 4-point path to blue because it has an even number of points, change the 5-point path to red, and leave the circle as it is:
* ![objects](images/Tut01-01.png)
* Select all objects to be changed by our new Effect extension:
* ![objects](images/Tut01-02.png)
* Pull down our extension form the Extensions | Effect submenu:
* ![objects](images/Tut01-03.png)
* You may get a popup about a deprecation warning, but all is well:
* ![objects](images/Tut01-04.png)
